--SOLUTION 1
CREATE OR REPLACE VIEW sales_revenue_by_category_qtr AS
SELECT c.category_id ,c.name AS category, SUM(p.amount) AS total_sales_revenue, 
CASE
    WHEN EXTRACT(MONTH FROM p.payment_date) IN (1, 2, 3) THEN EXTRACT(YEAR FROM p.payment_date)::TEXT || '-Q1'
    WHEN EXTRACT(MONTH FROM p.payment_date) IN (4, 5, 6) THEN EXTRACT(YEAR FROM p.payment_date)::TEXT || '-Q2'
    WHEN EXTRACT(MONTH FROM p.payment_date) IN (7, 8, 9) THEN EXTRACT(YEAR FROM p.payment_date)::TEXT || '-Q3'
    WHEN EXTRACT(MONTH FROM p.payment_date) IN (10, 11, 12) THEN EXTRACT(YEAR FROM p.payment_date)::TEXT || '-Q4'
END AS quarter
FROM category c
JOIN film_category fc ON fc.category_id = c.category_id
JOIN film f ON f.film_id = fc.film_id
JOIN inventory i ON i.film_id = f.film_id
JOIN rental r ON r.inventory_id = i.inventory_id
JOIN payment p ON p.rental_id = r.rental_id
GROUP BY c.category_id, c.name, quarter
HAVING SUM(p.amount) > 0;

SELECT *
FROM sales_revenue_by_category_qtr
WHERE quarter = '2017-Q1';
-----------------------------------------------------------------------------------------------
-- SOLUTION 2
CREATE OR REPLACE FUNCTION get_sales_revenue_by_category_qtr(current_quarter TEXT)
RETURNS TABLE (
    category_id category.category_id%TYPE,
    category_name category.name%TYPE,
    total_sales_revenue DECIMAL(10, 2),
    quarter TEXT
)
AS $$
BEGIN
    RETURN QUERY
    SELECT * 
	FROM sales_revenue_by_category_qtr
    WHERE sales_revenue_by_category_qtr.quarter = current_quarter;

    RETURN;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION get_sales_revenue_by_category_qtr(current_quarter TEXT) 

SELECT *
FROM get_sales_revenue_by_category_qtr('2017-Q2')
-----------------------------------------------------------------------------------------------
-- SOLUTION 3
CREATE OR REPLACE PROCEDURE new_movie(movie_title TEXT)
AS $$
DECLARE
  new_id INT;
BEGIN
  SELECT language_id INTO new_id
  FROM "language"
  WHERE name = 'Klingon';

  IF new_id IS NULL THEN
    RAISE EXCEPTION 'Language does not exist';
  END IF;

  INSERT INTO film (title, rental_rate, rental_duration, replacement_cost, release_year, language_id)
  VALUES (movie_title, 4.99, 3, 19.99, EXTRACT(YEAR FROM CURRENT_DATE), new_id)
  RETURNING film_id INTO new_id;

  IF new_id IS NULL THEN
    RAISE EXCEPTION 'Film already exists with given title';
  END IF;
END;
$$ LANGUAGE plpgsql;

--Exception Language does not exist
CALL new_movie('New Movie');
--Empty
SELECT * 
FROM film 
WHERE title = 'New Movie';
--Inserting the language
INSERT INTO "language" ("name")
VALUES ('Klingon');

--DONE
CALL new_movie('New Movie');

SELECT * 
FROM film 
WHERE title = 'New Movie';



